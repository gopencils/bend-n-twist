﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Spawner : MonoBehaviour {

    public GameObject[] obstacles;
    public GameObject enemy, token, pathColor;
    public int tokenSpawnFrequency = 8;
    public float timeBetweenObstacleSpawns, minTimeBetweenSpawns, offsetY = 0;
    int spawnCode = 0;
    //0 = normal, 1 = red, 2 = yellow, 3 = obstacle
    float randomRedLength;
    float randomYellowLength;
    float randomWhiteLength;
    float randomObstacleLength;
    float bonusLength;
    public float timeBetweenEnemies = 1.3f, firstEnemySpawn = 0.5f, firstObstacleSpawn = 1f, aheadOfPlayer = 100f;
    public PathFollow mapBuild;
    public PathFollow playerRun;
    public static bool isStop = true;
    public GameObject flag;
    public Texture finishTexture;

    void Start()
    {
        randomRedLength = Random.Range(5, 15);
        randomYellowLength = Random.Range(5, 15);
        randomWhiteLength = Random.Range(10, 20);
        randomObstacleLength = Random.Range(1, 3);
        bonusLength = 1;
        if (transform.gameObject.layer != LayerMask.NameToLayer("Enemy"))
            mapBuild.target = GameController.instance.currentMap;
        GetComponent<PathFollow>().alpha = 5;
        isStop = true;
        StartCoroutine(SpawnCr());
    }

    IEnumerator SpawnCr()
    {
        while(isStop || PlayerMovement.isPause)
        {
            yield return null;
        }
        if (spawnCode == 3)
        {
            GameObject tempObstacle = Instantiate(obstacles[Random.Range(0, obstacles.Length)], transform.position, transform.rotation);
            Vector3 obstPos = transform.position;
            obstPos.y += offsetY;
            tempObstacle.SetActive(true);
            if (transform.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                tempObstacle.tag = "Untagged";
            }
            var total = tempObstacle.transform.GetChild(0).transform.GetChild(0).transform.childCount;
            tempObstacle.transform.GetChild(0).transform.GetChild(0).transform.GetChild(Random.Range(0, total)).gameObject.SetActive(true);

            randomObstacleLength--;
            if (randomObstacleLength <= 0)
            {
                spawnCode = 0;
                yield return new WaitForSeconds(Random.Range(0.25f, 2f));
                if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                    StartCoroutine(SpawnCr());
            }
            else
            {
                yield return new WaitForSeconds(Random.Range(0.25f, 0.5f));
                if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                    StartCoroutine(SpawnCr());
            }

        }
        else if (spawnCode == 2)
        {
            spawnCode = 3;
            randomObstacleLength = Random.Range(1, 5);
            yield return new WaitForSeconds(Random.Range(0.25f, 2f));
            if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                StartCoroutine(SpawnCr());
        }
        else if (spawnCode == 1)
        {
            if (transform.gameObject.layer != LayerMask.NameToLayer("Enemy"))
            {
                GameObject tempObstacle = Instantiate(pathColor, transform.position, transform.rotation);
                Vector3 obstPos = transform.position;
                obstPos.y += offsetY;

                tempObstacle.tag = "Red";
                tempObstacle.GetComponent<MegaWorldPathDeform>().path = GameController.instance.currentMap;
                tempObstacle.GetComponent<MegaWorldPathDeform>().distance = mapBuild.distance;
                tempObstacle.GetComponent<MeshRenderer>().material.color = Color.red;
                tempObstacle.GetComponent<MegaWorldPathDeform>().stretch = randomRedLength;

                spawnCode = 2;
                yield return new WaitForSeconds(Random.Range(0.25f, 2f));
                tempObstacle.GetComponent<MegaModifyObject>().recalcCollider = false;
                tempObstacle.GetComponent<MegaModifyObject>().recalcbounds = false;
                if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                    StartCoroutine(SpawnCr());
            }
            else
            {
                spawnCode = 2;
                yield return new WaitForSeconds(Random.Range(0.25f, 2f));
                if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                    StartCoroutine(SpawnCr());
            }
        }
        else
        {
            randomRedLength = Random.Range(5f, 10f);
            spawnCode = 1;
            yield return new WaitForSeconds(Random.Range(0.25f, 2f));
            if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                StartCoroutine(SpawnCr());
        }
    }

    IEnumerator SpawnBonusCr()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject tempObstacle = Instantiate(pathColor, transform.position, transform.rotation);
        Vector3 obstPos = transform.position;
        obstPos.y += offsetY;
        tempObstacle.tag = "Bonus";
        tempObstacle.GetComponent<MegaWorldPathDeform>().path = GameController.instance.currentMap;
        tempObstacle.GetComponent<MegaWorldPathDeform>().distance = mapBuild.distance;
        tempObstacle.GetComponent<MeshRenderer>().material.color = Color.green;
        tempObstacle.GetComponent<MegaWorldPathDeform>().stretch = 10;
        tempObstacle.name = bonusLength.ToString();
        tempObstacle.GetComponent<MegaModifyObject>().recalcCollider = false;
        tempObstacle.GetComponent<MegaModifyObject>().recalcbounds = false;
        GameObject flagSpawn = Instantiate(flag, transform.position, transform.rotation);
        flagSpawn.transform.DOMoveX(flagSpawn.transform.position.x + 2, 0);
        flagSpawn.GetComponentInChildren<Text>().text = "X" + bonusLength.ToString();
        if(bonusLength == 1)
        {
            flagSpawn.transform.DOMoveX(flagSpawn.transform.position.x + 2, 0);
            //flagSpawn.transform.DOMoveY(flagSpawn.transform.position.y - 5, 0);
            //flagSpawn.transform.DOMoveY(flagSpawn.transform.position.y + 5, 2);
            flagSpawn.GetComponentInChildren<Text>().text = "FINISH";
            tempObstacle.tag = "FinishLine";
            tempObstacle.GetComponent<MeshRenderer>().material.color = Color.black;
            tempObstacle.GetComponent<MeshRenderer>().material.mainTexture = finishTexture;
            tempObstacle.GetComponent<MegaWorldPathDeform>().stretch = 20;
            yield return new WaitForSeconds(3);
        }
        else
        {
            flagSpawn.transform.GetChild(1).gameObject.SetActive(false);
            flagSpawn.GetComponentInChildren<MeshRenderer>().enabled = false;
            flagSpawn.GetComponentInChildren<Text>().transform.DOPunchScale(Vector3.one * 0.6f, 0.25f).SetLoops(-1, LoopType.Yoyo);
        }

        if (bonusLength < 10)
        {
            bonusLength++;
            tempObstacle.GetComponent<MegaModifyObject>().recalcCollider = false;
            tempObstacle.GetComponent<MegaModifyObject>().recalcbounds = false;
            if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                StartCoroutine(SpawnBonusCr());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Finish"))
        {
            isStop = true;
            //GameController.isFinish = true;
            //GameController.instance.isStartGame = false;
            StartCoroutine(SpawnBonusCr());
        }
    }
}
