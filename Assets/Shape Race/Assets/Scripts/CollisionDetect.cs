﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CollisionDetect : MonoBehaviour {

    public Mesh[] meshes;
    public ParticleSystem tokenParticle;

    [HideInInspector]
    public bool gameIsOver = false;

    private MeshFilter playerMesh;
    //private ParticleSystem playerParticle;
    private Animation cameraAnim;
    bool isHit = false;


    void Start () {
        //Initizalization
        playerMesh = GetComponent<MeshFilter>();
        //playerParticle = GetComponent<ParticleSystem>();
        cameraAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animation>();
        isHit = false;
	}
	
    public void OnTriggerStay(Collider other)
    {
        if(!isHit)
        {
            if (other.CompareTag("Obstacle"))         //If Player collided with an obstacle
            {
                //playerParticle.Play();
                if (PlayerMovement.worldPathDeform.speed > PlayerMovement.defaultSpeed)
                {
                    other.GetComponent<BoxCollider>().enabled = false;
                    other.GetComponent<ParticleSystem>().Play();
                    if (transform.GetComponent<MegaWorldPathDeform>().stretch < 6)
                    {
                        transform.GetComponent<MegaWorldPathDeform>().stretch += 0.5f;
                    }
                    var crew = other.transform.GetChild(0);
                    crew.GetComponent<Animator>().SetTrigger("IsJumping");
                    crew.parent = transform;
                    var getIndex = transform.childCount - 1;
                    var posX = transform.GetChild(0).localPosition.x;
                    var posY = transform.GetChild(0).localPosition.y;
                    var posZ = transform.GetChild(0).localPosition.z - 3 * getIndex;
                    other.GetComponent<MeshRenderer>().enabled = false;
                    crew.DOMoveY(crew.transform.position.y + 4f, 0.4f).OnComplete(() =>
                    {
                        crew.DOLocalMove(new Vector3(posX, posY, posZ), 0.6f).SetEase(Ease.OutSine).OnComplete(() =>
                        {
                            crew.GetComponent<MegaAttach>().target = transform.GetChild(0).GetComponent<MegaModifyObject>();
                            crew.DOLocalRotateQuaternion(Quaternion.Euler(0, 150, 0), 0.2f).OnComplete(() =>
                          {
                              crew.transform.localPosition = new Vector3(posX, posY, posZ);
                              crew.transform.localEulerAngles = new Vector3(0, 150, 0);
                          //crew.GetComponent<MegaAttach>().attached = true;
                      });
                        });
                    });
                    GameController.instance.Scoring();
                    GameController.instance.ChangeCrew(1);
                    cameraAnim.Play();      //Plays the animation attached to the Main Camera
                }
                else
                {
                    PlayerMovement.isPause = true;
                    PlayerMovement.worldPathDeform.speed = 0;
                    other.GetComponent<ParticleSystem>().Play();
                    if (transform.GetComponent<MegaWorldPathDeform>().stretch > 4)
                    {
                        transform.GetComponent<MegaWorldPathDeform>().stretch -= 0.5f;
                    }
                    var totalChild = transform.childCount;
                    if (totalChild > 1)
                    {
                        var crew = transform.GetChild(0);
                        crew.GetComponent<Animator>().SetTrigger("IsJumping");
                        //other.GetComponent<MeshRenderer>().enabled = false;
                        crew.DOMoveY(crew.transform.position.y + 10f, 0.4f).OnComplete(() =>
                        {
                            Destroy(crew.gameObject);
                        });
                        GameController.instance.ChangeCrew(-1);
                        foreach (Transform child in transform)
                        {
                            child.transform.DOLocalMoveZ(child.transform.localPosition.z + 3, 0.25f);
                        }
                    }
                    else
                    {
                        GameController.instance.Lose();
                    }
                    //PlayerMovement.worldPathDeform.speed -= 0.2f;
                    //if(PlayerMovement.worldPathDeform.speed <= 0)
                    //{
                    //    GameController.instance.Lose();
                    //}
                    cameraAnim.Play();      //Plays the animation attached to the Main Camera
                    isHit = true;
                    StartCoroutine(delayFallback());
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bonus"))      //If Player collided with an enemy
        {
            //playerParticle.Play();      //Plays playerParticle
            GameController.instance.Bonus(int.Parse(other.name));
            //cameraAnim.Play();      //Plays the animation attached to the Main Camera
        }
    }

    IEnumerator delayFallback()
    {
        yield return new WaitForSeconds(0.5f);
        isHit = false;
    }

    public void ChangeMesh()
    {
        //Selects a random mesh from the meshes list until it is a different mesh than the player's mesh. Then changes player' s mesh to the selected one
        int index;
        do
        {
            index = Random.Range(0, meshes.Length);
        } while (playerMesh.mesh.name == meshes[index].name + " Instance");
        playerMesh.mesh = meshes[index];
    }
}
