﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    public static FollowPath instance;
    public Transform targetFollow;
    public PathFollow followPath;

    void Start()
    {
        instance = this;
        //followPath = GetComponent<PathFollow>();
        if(transform.gameObject.layer != LayerMask.NameToLayer("Enemy"))
        followPath.target = GameController.instance.currentMap;
    }

    void Update()
    {
        if (CompareTag("Spawner"))
        {
            followPath.distance = targetFollow.GetComponent<MegaWorldPathDeform>().distance + 5;
        }
        else
        {
            followPath.distance = targetFollow.GetComponent<MegaWorldPathDeform>().distance;
        }
    }

    public void ChangeTargetFollow(Transform target)
    {
        targetFollow = target;
    }
}
