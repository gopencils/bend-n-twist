﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement instance;
    private Rigidbody rb;
    public GameObject target;
    public static float defaultSpeed;
    float a = 0;
    public static MegaWorldPathDeform worldPathDeform;
    int speedBonus = 1;
    public ParticleSystem hitEffect;
    bool isRed = false;
    public static bool isPause = false;

    void Start()
    {
        //Initializations\
        worldPathDeform = GetComponent<MegaWorldPathDeform>();
        worldPathDeform.path = GameController.instance.currentMap;
        defaultSpeed = worldPathDeform.speed;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //transform.rotation = transform.parent.rotation;
        if (GameController.instance.isStartGame)
        {
            if (Input.GetMouseButtonDown(0))
            {
                foreach (Transform child in transform)
                {
                    if (!child.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Jump"))
                    {
                        child.transform.DOKill();
                        child.transform.DOLocalMoveY(1f, 0);
                        child.transform.localEulerAngles = new Vector3(0, 150, 0);
                        child.transform.DOLocalMoveY(3, 0.3f).SetLoops(2, LoopType.Yoyo);
                        child.transform.DOBlendableRotateBy(new Vector3(0, 180, 0), 0.3f).SetEase(Ease.Linear).OnComplete(() =>
                          {
                              child.transform.DOBlendableRotateBy(new Vector3(0, 180, 0), 0.3f).SetEase(Ease.Linear);
                          });
                        child.GetComponent<Animator>().SetTrigger("IsRunning");
                    }
                }
            }
            if (Input.GetMouseButton(0))
            {
                if (isPause)
                {
                    isPause = false;
                    worldPathDeform.speed = defaultSpeed;
                }
                if (isRed)
                {
                    if (worldPathDeform.speed > 1)
                    {
                        worldPathDeform.speed -= 2 * speedBonus * Time.deltaTime;
                        hitEffect.Play();
                    }
                }
                else
                {
                    if (transform.GetComponent<MegaTwist>().angle > -500)
                    {
                        transform.GetComponent<MegaTwist>().angle -= 20;
                    }
                    if (a < 720)
                    {
                        a += 20;
                    }
                    worldPathDeform.rotate += a * Time.deltaTime;
                    if (worldPathDeform.speed < defaultSpeed * 1.5f)
                    {
                        worldPathDeform.speed += 2 * speedBonus * Time.deltaTime;
                    }
                }
            }
            else
            {
                if (a > 0)
                {
                    a -= 10;
                }
                worldPathDeform.rotate += a * Time.deltaTime;
                if (transform.GetComponent<MegaTwist>().angle < 0)
                {
                    transform.GetComponent<MegaTwist>().angle += 20;
                }
                //worldPathDeform.rotate = Mathf.MoveTowards(worldPathDeform.rotate, 0, 5*Time.deltaTime);
                if(!isPause)
                worldPathDeform.speed = Mathf.MoveTowards(worldPathDeform.speed, defaultSpeed, 1 * Time.deltaTime);
            }
            if (Input.GetMouseButtonUp(0))
            {
                foreach (Transform child in transform)
                {
                    if (!child.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle") && !child.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Running") && !child.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Jump"))
                    {
                        child.GetComponent<Animator>().SetTrigger("isIdle");
                    }
                }
            }
        }
        else
        {
            if (!isPause)
            {
                if (transform.GetComponent<MegaTwist>().angle > -500)
                {
                    transform.GetComponent<MegaTwist>().angle -= 20;
                }
                if (a < 720)
                {
                    a += 20;
                }
                worldPathDeform.rotate += a * Time.deltaTime;
            }
            //if (worldPathDeform.speed < 2.5f)
            //{
            //    worldPathDeform.speed += 2 * speedBonus * Time.deltaTime;
            //}
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Red"))
        {
            isRed = true;
            speedBonus = 1;
            hitEffect.startColor = Color.red;
            //hitEffect.Play();
            //Debug.Log("Red");
        }
        if(other.CompareTag("Yellow"))
        {
            speedBonus = 2;
            hitEffect.startColor = Color.yellow;
            hitEffect.Play();
            Debug.Log("Yellow");
        }
        if (other.CompareTag("Bonus"))
        {
            //speedBonus = 2;
            hitEffect.startColor = Color.green;
            hitEffect.Play();
            //Debug.Log("Bonus");
        }
        if(other.CompareTag("FinishLine"))
        {
            GameController.instance.FinishMove();
        }
        if (other.CompareTag("Obstacle"))
        {
            //speedBonus = 2;
            hitEffect.startColor = Color.white;
            hitEffect.Play();
            //Debug.Log("Bonus");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Red") || other.CompareTag("Yellow"))
        {
            isRed = false;
            speedBonus = 1;
            hitEffect.Stop();
            Destroy(other.gameObject, 2);
            //Debug.LogError("Stop");
        }
    }
}