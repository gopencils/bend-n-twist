﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Rendering;

public class UnityAdsManager : MonoBehaviour, IUnityAdsListener
{
    string playStore_ID = "1595694";
    string appStore_ID = "1595695";

    public bool isPlayStore;
    public bool isTestAd;

    public static UnityAdsManager Instance { get => Instance; private set => Instance = value; }

    private string videoAd = "video";
    private string rewardedVideoAd = "rewardedVideo";

    private void Start()
    {
#if UNITY_ANDROID
        Advertisement.Initialize(playStore_ID, isTestAd);
#elif UNITY_IOS
        Advertisement.Initialize(appStore_ID, isTestAd);
#endif
    }

    public void ShowVideoAd()
    {
        if (!Advertisement.IsReady(videoAd)) { return; }
        Advertisement.Show(videoAd);
    }

    public void ShowRewardedVideoAd()
    {
        if (!Advertisement.IsReady(rewardedVideoAd)) { return; }
        Advertisement.Show(rewardedVideoAd);
    }

    public void OnUnityAdsReady(string placementId)
    {
        Debug.Log("Ad ready!");
    }

    void IUnityAdsListener.OnUnityAdsDidError(string message)
    {
        throw new System.NotImplementedException();
    }

    void IUnityAdsListener.OnUnityAdsDidStart(string placementId)
    {
        throw new System.NotImplementedException();
    }

    void IUnityAdsListener.OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        throw new System.NotImplementedException();
    }
}
