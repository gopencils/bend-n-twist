﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using UnityEngine.EventSystems;
using System.Linq;
using VisCircle;
using UnityEngine.PostProcessing;
using UnityStandardAssets.Utility;

public class GameController : MonoBehaviour
{
    [Header("Variable")]
    public static GameController instance;
    public int maxLevel;
    public bool isStartGame = false;
    public bool isControl = true;
    bool isVibrate = false;
    public GameObject feverText;
    float maxPlusEffect;
    public string playerName;
    public int crewNumber;
    public static bool isFinish;

    [Header("UI")]
    public Slider progress;
    public Slider progress1;
    public Slider progress2;
    public Text progressName;
    public Text progressName1;
    public Text progressName2;
    string randomName;
    public GameObject winPanel;
    public GameObject losePanel;
    public Text currentLevelTextBig;
    public Text currentLevelText;
    public Text nextLevelText;
    public int currentLevel;
    public Canvas canvas;
    public GameObject startGameMenu;
    public GameObject shopMenu;
    public Transform sliderTab;
    public Transform talentTab;
    public Text title;
    static int currentBG = 0;
    public InputField levelInput;
    public Image compliment;
    public List<Sprite> listCompliment = new List<Sprite>();
    public Text taskText;
    public GameObject tutorial;
    public GameObject startButton;
    public Text coinText;
    int coin;
    int tempCoin;
    public List<CoinFlyAnimation> listCoinAnim = new List<CoinFlyAnimation>();
    public Text crewText;

    [Header("Objects")]
    public GameObject mainChar;
    public GameObject plusVarPrefab;
    public GameObject conffeti;
    GameObject conffetiSpawn;
    public List<GameObject> listLevel = new List<GameObject>();
    public List<GameObject> listShopTalent = new List<GameObject>();
    public List<Mesh> listShopSlider = new List<Mesh>();
    public List<GameObject> listEnv = new List<GameObject>();
    public MegaShapeCircle currentMap;
    public GameObject blast;
    public GameObject environment;
    public GameObject dieEffect;
    public GameObject finishTutorial;
    public Slider powerBar;
    public GameObject enemy1;
    public GameObject enemy2;
    public List<GameObject> listResult = new List<GameObject>();
    public List<Sprite> listFlag = new List<Sprite>();
    public GameObject smallPart;
    public Transform rocket;

    private void OnEnable()
    {
        // PlayerPrefs.DeleteAll();
        Application.targetFrameRate = 60;
        instance = this;
        isFinish = false;
        maxLevel = listLevel.Count - 1;
        playerName = PlayerPrefs.GetString("playerName");
        levelInput.text = playerName;
        progressName.text = playerName;
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        currentMap = listLevel[currentLevel].GetComponent<MegaShapeCircle>();
        listEnv[Random.Range(0, listEnv.Count)].SetActive(true);
        //currentLevelTextBig.text = "LEVEL " + (currentLevel + 1).ToString();
        //currentLevelText.text = currentLevel.ToString();
        //nextLevelText.text = (currentLevel + 1).ToString();
        listLevel[currentLevel].SetActive(true);
        startGameMenu.SetActive(true);
        title.DOColor(new Color32(255, 255, 255, 0), 3);
        isControl = true;
        coin = PlayerPrefs.GetInt("Coin");
        coinText.text = coin.ToString();
        progress.value = 0;
        crewNumber = 1;
        //listLevel.RemoveAt(currentLevel);
        //enemy1.GetComponent<MegaWorldPathDeform>().path = listLevel[0].GetComponent<MegaShapeCircle>();
        //listLevel[0].SetActive(true);
        //listLevel[0].GetComponent<MegaShapeCircle>().BuildTubeMesh();
        //listLevel.RemoveAt(0);
        //enemy2.GetComponent<MegaWorldPathDeform>().path = listLevel[0].GetComponent<MegaShapeCircle>();
        //listLevel[0].SetActive(true);
        //listLevel[0].GetComponent<MegaShapeCircle>().BuildTubeMesh();
        //listLevel.RemoveAt(0);
        currentMap.BuildTubeMesh();
        StartCoroutine(delayStart());
    }

    IEnumerator delayStart()
    {
        yield return new WaitForSeconds(1f);
        RandomNameGenerator();
        progressName1.text = randomName;
        RandomNameGenerator();
        progressName2.text = randomName;
        //currentMap.MakeShape();
    }

    float h, v;
    Vector3 firstP, lastP, dir;
    public Transform target;

    private void Update()
    {
        if (isStartGame)
        {
            progress.value = PlayerMovement.worldPathDeform.distance;
            if (progress.value > progress.maxValue / 2 && (progress.value < progress1.value || progress.value < progress2.value))
            {
                progress1.value += Random.Range(0.005f, 0.015f);
                progress2.value += Random.Range(0.01f, 0.02f);
            }
            else
            {
                progress1.value += Random.Range(0.01f, 0.02f);
                progress2.value += Random.Range(0.02f, 0.03f);
            }
        }
        //if (!isStartGame && isControl)
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        ButtonStartGame();
        //    }
        //}
        if(isFinish)
        {
            powerBar.value -= 0.5f;
            if(Input.GetMouseButtonDown(0))
            {
                powerBar.value+=10;
            }
            PlayerMovement.worldPathDeform.speed = 1 + powerBar.value / 75;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    IEnumerator PlusEffect(Vector3 pos)
    {
        maxPlusEffect++;
        if (!UnityEngine.iOS.Device.generation.ToString().Contains("5") && !isVibrate)
        {
            isVibrate = true;
            StartCoroutine(delayVibrate());
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }
        var plusVar = Instantiate(plusVarPrefab);
        plusVar.transform.SetParent(canvas.transform);
        plusVar.transform.localScale = new Vector3(1, 1, 1);
        plusVar.transform.position = new Vector3(pos.x + Random.Range(-50, 50), pos.y + Random.Range(-100, -75), pos.z);
        plusVar.GetComponent<Text>().DOColor(new Color32(255, 255, 255, 0), 1f);
        plusVar.SetActive(true);
        plusVar.transform.DOMoveY(plusVar.transform.position.y + Random.Range(50, 90), 0.5f);
        Destroy(plusVar, 0.5f);
        yield return new WaitForSeconds(0.01f);
        maxPlusEffect--;
    }

    IEnumerator delayVibrate()
    {
        yield return new WaitForSeconds(0.2f);
        isVibrate = false;
    }

    public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        return parentCanvas.transform.TransformPoint(movePos);
    }

    public void ButtonStartGame()
    {
        startGameMenu.SetActive(false);
        isStartGame = true;
        //tutorial.SetActive(false);
        mainChar.GetComponent<MegaWorldPathDeform>().animate = true;
        Spawner.isStop = false;
        crewText.gameObject.SetActive(true);
        enemy1.GetComponent<MegaWorldPathDeform>().animate = true;
        enemy2.GetComponent<MegaWorldPathDeform>().animate = true;
    }

    public void ChangeCrew(int change)
    {
        crewNumber += change;
        if(crewNumber < 0)
        {
            crewNumber = 0;
        }
        crewText.text = /*"CREW: " + */crewNumber.ToString();
    }

    bool isCheckShop = false;
    public void ButtonShopMenu(int shopType)
    {
        shopMenu.SetActive(true);
        if (!isCheckShop)
        {
            //isCheckShop = true;
            //for (int i = 0; i < listShopTalent.Count; i++)
            //{
            //    var checkAgain = PlayerPrefs.GetInt("Talent" + i.ToString());
            //    if (checkAgain == 1)
            //    {
            //        var checkCurrent = PlayerPrefs.GetInt("CurrentTalent");
            //        if (i == checkCurrent)
            //        {
            //            listShopTalent[i].transform.GetChild(checkAgain).SetAsLastSibling();
            //        }
            //        else
            //        {
            //            PlayerPrefs.SetInt("Talent" + i.ToString(), 2);
            //            listShopTalent[i].GetComponent<ItemManager>().listStatus[checkAgain].SetSiblingIndex(1);
            //        }
            //    }
            //    else
            //    {
            //        listShopTalent[i].transform.GetChild(checkAgain).SetAsLastSibling();
            //    }
            //}

            //for (int i = 0; i < listShopSlider.Count; i++)
            //{
            //    var checkAgain = PlayerPrefs.GetInt("Slider" + i.ToString());
            //    if (checkAgain == 1)
            //    {
            //        var checkCurrent = PlayerPrefs.GetInt("CurrentSlider");
            //        if (i == checkCurrent)
            //        {
            //            listShopSlider[i].transform.GetChild(checkAgain).SetAsLastSibling();
            //        }
            //        else
            //        {
            //            PlayerPrefs.SetInt("Slider" + i.ToString(), 2);
            //            listShopSlider[i].GetComponent<ItemManager>().listStatus[checkAgain].SetSiblingIndex(1);
            //        }
            //    }
            //    else
            //    {
            //        listShopSlider[i].transform.GetChild(checkAgain).SetAsLastSibling();
            //    }
            //}
            if(shopType == 1)
            {
                SliderTabButton();
            }
            else
            {
                TalentTabButton();
            }
        }
    }

    public void ExitShop()
    {
        shopMenu.SetActive(false);
    }

    public void SliderTabButton()
    {
        sliderTab.SetAsLastSibling();
    }

    public void TalentTabButton()
    {
        talentTab.SetAsLastSibling();
    }

    public void Win()
    {
        StopAllCoroutines();
        StartCoroutine(DelayWin());
    }

    IEnumerator DelayWin()
    {
        //if (isStartGame)
        //{
        isStartGame = false;
        isControl = false;
        tutorial.SetActive(false);
        losePanel.SetActive(false);
        currentLevel++;
        if (currentLevel > maxLevel)
        {
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        crewText.gameObject.SetActive(false);
        for (int i = 0; i < listResult.Count; i++)
        {
            if (i != 0)
            {
                listResult[i].transform.GetChild(1).GetComponent<Image>().sprite = listFlag[Random.Range(0, listFlag.Count)];
                if (i == 1)
                    listResult[i].transform.GetChild(2).GetComponent<Text>().text = progressName1.text;
                if (i == 2)
                    listResult[i].transform.GetChild(2).GetComponent<Text>().text = progressName2.text;
            }
            else
            {
                listResult[i].transform.GetChild(2).GetComponent<Text>().text = playerName;
            }
        }
        yield return new WaitForSeconds(2);
        winPanel.SetActive(true);
        //var nextButton = winPanel.transform.GetChild(2).gameObject;
        //nextButton.SetActive(false);
        //var textWinCoin = winPanel.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>();
        //textWinCoin.text = tempCoin.ToString();
        //blast.SetActive(false);
        //blast.transform.position = new Vector3(mainChar.transform.position.x, 20, mainChar.transform.position.z);
        //blast.SetActive(true);
        yield return new WaitForSeconds(1);
        foreach (var item in listCoinAnim)
        {
            item.Move();
        }
        //textWinCoin.DOText(" 0 ", 1.2f, true, ScrambleMode.Numerals);
        yield return new WaitForSeconds(1);
        coin += tempCoin;
        PlayerPrefs.SetInt("Coin", coin);
        coinText.text = coin.ToString();
        coinText.transform.parent.DOScale(Vector3.one * 1.2f, 0.2f).SetLoops(2, LoopType.Yoyo);
        //nextButton.SetActive(true);
        //}
    }

    public void Lose()
    {
        if (isStartGame)
        {
            Debug.Log("Lose");
            StopAllCoroutines();
            tutorial.SetActive(false);
            isStartGame = false;
            isControl = false;
            StartCoroutine(DelayLose());
        }
    }

    IEnumerator DelayLose()
    {
        yield return new WaitForSeconds(1);
        Time.timeScale = 0;
        losePanel.SetActive(true);
    }

    public void LoadScene()
    {
        Time.timeScale = 1;
        StartCoroutine(delayLoadScene());
    }

    IEnumerator delayLoadScene()
    {
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        var temp = conffetiSpawn;
        Destroy(temp);
        SceneManager.LoadScene(0);
    }

    public void FinishMove()
    {
        finishTutorial.SetActive(true);
        isFinish = true;
        isStartGame = false;
    }

    public void Scoring()
    {
        coin++;
        coinText.text = coin.ToString();
        PlayerPrefs.SetInt("Coin", coin);
    }

    public void Bonus(int bonus)
    {
        isFinish = false;
        isStartGame = false;
        finishTutorial.SetActive(false);
        coin*=bonus;
        coinText.text = coin.ToString();
        PlayerPrefs.SetInt("Coin", coin);
        StartCoroutine(slowDown());
    }

    IEnumerator slowDown()
    {
        var totalChild = mainChar.transform.childCount;
        PlayerMovement.worldPathDeform.gizmoScale = Vector3.zero;
        for (int i = 0; i < totalChild - 1; i++)
        {
            var spawn = Instantiate(smallPart);
            spawn.transform.parent = mainChar.transform.parent;
            spawn.SetActive(true);
            spawn.GetComponent<PathFollow>().target = currentMap;
            spawn.GetComponent<PathFollow>().distance = PlayerMovement.worldPathDeform.distance - i/5;
            spawn.GetComponent<PathFollow>().speed = PlayerMovement.worldPathDeform.speed / 2;
            spawn.transform.parent = rocket.transform;
            var targetChar = mainChar.transform.GetChild(0).transform;
            targetChar.parent = spawn.transform;
            targetChar.transform.localPosition = new Vector3(0, 1, 0);
            targetChar.transform.localEulerAngles = new Vector3(0, 150, 0);
        }
        PlayerMovement.worldPathDeform.speed /= 3;
        yield return new WaitForSeconds(0.5f);
        PlayerMovement.worldPathDeform.speed *= 3;
        for (int i = rocket.childCount - 1; i > 0; i--)
        {
            yield return new WaitForSeconds(0.15f);
            DOTween.To(() => rocket.transform.GetChild(i).GetComponent<PathFollow>().speed, x => rocket.transform.GetChild(i).GetComponent<PathFollow>().speed = x, 0, 0.3f).OnComplete(() =>
            {
                if (i != 0)
                {
                    var crew = rocket.transform.GetChild(i).transform.GetChild(0);
                    crew.GetComponent<Animator>().SetTrigger("IsJumping");
                    crew.transform.DOMoveY(crew.transform.position.y + 1000, 100);
                }
            });
        }
        mainChar.GetComponent<SphereCollider>().enabled = false;
        //PlayerMovement.worldPathDeform.animate = false;
        yield return new WaitForSeconds(3);
        PlayerMovement.worldPathDeform.animate = false;
        Win();
    }

    public void OnChangeName()
    {
        if (levelInput != null)
        {
            playerName = levelInput.text.ToString();
            PlayerPrefs.SetString("playerName", playerName);
        }
    }

    public void OnChangeMap()
    {
        if (levelInput != null)
        {
            int level = int.Parse(levelInput.text.ToString());
            if (level <= maxLevel)
            {
                PlayerPrefs.SetInt("currentLevel", level);
                SceneManager.LoadScene(0);
            }
        }
    }

    public void ButtonNextLevel()
    {
        title.DOKill();
        isStartGame = true;
        currentLevel++;
        if (currentLevel > maxLevel)
        {
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        SceneManager.LoadScene(0);
    }

    public void ChooseTalentButton()
    {
        int index = int.Parse(EventSystem.current.currentSelectedGameObject.name);
        foreach(Transform child in mainChar.transform.GetChild(0).transform.GetChild(0).transform)
        {
            child.gameObject.SetActive(false);
        }
        listShopTalent[index].SetActive(true);
        //int check = PlayerPrefs.GetInt("Talent" + index.ToString());
        //if(check == 0)
        //{
        //    var objectTarget = EventSystem.current.currentSelectedGameObject.GetComponent<ItemManager>().listStatus[0];
        //    string priceS = objectTarget.transform.GetChild(0).GetComponent<Text>().text;
        //    int price = int.Parse(priceS);
        //    if (coin >= price)
        //    {
        //        coin -= price;
        //        PlayerPrefs.SetInt("Coin", coin);
        //        PlayerPrefs.SetInt("Talent" + index.ToString(), 1);
        //        objectTarget.transform.SetSiblingIndex(0);
        //        PlayerPrefs.SetInt("CurrentTalent", index);
        //    }
        //}
        //if(check == 1)
        //{
        //    PlayerPrefs.SetInt("CurrentTalent", index);
        //    PlayerPrefs.SetInt("Talent" + index, 2);
        //}
    }

    public void ChooseSliderButton()
    {
        int index = int.Parse(EventSystem.current.currentSelectedGameObject.name);
        mainChar.GetComponent<MeshFilter>().sharedMesh = listShopSlider[index];
        mainChar.GetComponent<MegaModifyObject>().MeshUpdated();
        //int check = PlayerPrefs.GetInt("Slider" + index.ToString());
        //if (check == 0)
        //{
        //    var objectTarget = EventSystem.current.currentSelectedGameObject.transform.GetComponent<ItemManager>().listStatus[0];
        //    string priceS = objectTarget.transform.GetChild(0).GetComponent<Text>().text;
        //    int price = int.Parse(priceS);
        //    if (coin >= price)
        //    {
        //        coin -= price;
        //        PlayerPrefs.SetInt("Coin", coin);
        //        PlayerPrefs.SetInt("Slider" + index.ToString(), 1);
        //        objectTarget.transform.SetSiblingIndex(0);
        //        PlayerPrefs.SetInt("CurrentSlider", index);
        //    }
        //}
        //if (check == 2)
        //{
        //    PlayerPrefs.SetInt("CurrentBall", index);
        //    PlayerPrefs.SetInt("Ball" + index, 1);
        //}
    }

    void RandomNameGenerator()
    {
        string[] nameComponent1 = new string[] { "CS2D rocks!",
"PlayWithWheel",
"PlayWithPentablet",
"PlayWithJoystick",
"PlayWithOculusRift",
"D0om",
"L0lz0r",
"ROFLCopter",
"ZOMG",
"WTF",
"0_o",
"pwn3d",
"pwner0r",
"0wn4ge",
"no0b",
"Player",
"Master of Disaster",
"Gun",
"MyAss",
"Carnage",
"Jack",
"Tom",
"Pete",
"Rob",
"Steve",
"Chris",
"Jon",
"Bloodlord",
"Mr Mafia",
"The Bullet",
"Mac Man",
"Uber AI",
"Uber Killer",
"Executor",
"Assassin",
"Cutthroat",
"Slayer",
"Gladiator",
"Rogue",
"Trooper",
"Soldier",
"Infernal",
"Feuersturm",
"Der Vollstrecker",
"Keksmeister",
"I Killer",
"Linux Lover",
"Ping of Death",
"lawlz",
"LUL",
"l0rl",
"fucktard",
"CS2D.com",
"USGN.de",
"-_-",
"m00h",
"yeah",
"WAR!",
"Deagle Master",
"HE Overkill",
"Nightblaster",
"Starfish",
"Blowball",
"Supreme",
"AgainstCheaters",
"TheMachine",
"Circuit",
"Scrap",
"Drone",
"Robo",
"NoIntelligence",
"Cheater_Little",
"Hacker_Little",
"Stack Overflow",
"Albert",
"Allen",
"Bert",
"Bob",
"Cecil",
"Clarence",
"Elliot",
"Elmer",
"Ernie",
"Eugene",
"Fergus",
"Ferris",
"Frank",
"Frasier",
"Fred",
"George",
"Graham",
"Harvey",
"Irwin",
"Larry",
"Lester",
"Marvin",
"Neil",
"Niles",
"Oliver",
"Opie",
"Ryan",
"Toby",
"Ulric",
"Ulysses",
"Uri",
"Waldo",
"Wally",
"Walt",
"Wesley",
"Yanni",
"Yogi",
"Yuri",
"111111",
"123456",
"12345678",
"abc123",
"abramov",
"account",
"accounting",
"ad",
"adm",
"admin",
"administrator",
"adver",
"advert",
"advertising",
"afanasev",
"agafonov",
"agata",
"aksenov",
"aleksander",
"aleksandrov",
"alekse",
"alenka",
"alexe",
"alexeev",
"alla",
"anatol",
"andre",
"andreev",
"andrey",
"anna",
"anya",
"ao",
"aozt",
"arhipov",
"art",
"avdeev",
"avto",
"bank",
"baranov",
"Baseball",
"belousov",
"bill",
"billing",
"blinov",
"bobrov",
"bogdanov",
"buh",
"buhg",
"buhgalter",
"buhgalteria",
"business",
"bux",
"catchthismail",
"company",
"contact",
"contactus",
"corp",
"design",
"dir",
"director",
"direktor",
"dragon",
"economist",
"edu",
"email",
"er",
"expert",
"export",
"fabrika",
"fin",
"finance",
"ftp",
"glavbuh",
"glavbux",
"glbuh",
"helloitmenice",
"help",
"holding",
"home",
"hr",
"info",
"ingthisleter",
"job",
"john",
"kadry",
"letmein",
"mail",
"manager",
"marketing",
"marketing",
"mike",
"mogggnomgon",
"monkey",
"moscow",
"mysql",
"office",
"ok",
"oracle",
"password",
"personal",
"petgord34truew",
"post",
"postmaster",
"pr",
"qwerty",
"rbury",
"reklama",
"root",
"root",
"sale",
"sales",
"secretar",
"sekretar",
"support",
"test",
"testing",
"thisisjusttestletter",
"trade",
"uploader",
"user",
"webmaster",
"www-data",
"Thomas",
"William",
"Jacob",
"Liam",
"Felix",
"Nathan",
"Samuel",
"Logan",
"Alexis",
"Noah",
"Olivier",
"Raphael",
"Gabriel",
"Emile",
"Leo",
"Charles",
"Antoine",
"Benjamin",
"Adam",
"Édouard",
"Xavier",
"Victor",
"Zack",
"Mathis",
"Jayden",
"Theo",
"Elliot",
"Zachary",
"Louis",
"James",
"Anthony",
"Alexandre",
"Lucas",
"Justin",
"Arthur",
"Tristan",
"Loïc",
"Ethan",
"Henri",
"Nolan",
"Nicolas",
"Arnaud",
"Jeremy",
"Hugo",
"Dylan",
"Laurent",
"Eli",
"Vincent",
"Isaac",
"Etienne",
"Philippe",
"Malik",
"David",
"Alex",
"Ryan",
"Maxime",
"Hubert",
"Ludovic",
"Eloi",
"Damien",
"Hayden",
"Evan",
"Zackary",
"Milan",
"Mathéo",
"Jules",
"Simon",
"Caleb",
"Rayan",
"Eliot",
"Rafael",
"Derek",
"Tyler",
"Eliott",
"Matteo",
"Jake",
"Jordan",
"Loik",
"Michael",
"Louka",
"Jackson",
"Julien",
"Leonard",
"Joshua",
"Daniel",
"Mathias",
"Emrick",
"Lyam",
"Mayson",
"Brandon",
"Mathieu",
"Rémi",
"Sam",
"Aiden",
"Alexander",
"Theodore",
"Enzo",
"Mikael",
"Christophe",
"Tommy",
"Oliver",
"George",
"Harry",
"Jack",
"Jacob",
"Noah",
"Charlie",
"Muhammad",
"Thomas",
"Oscar",
"William",
"James",
"Henry",
"Leo",
"Alfie",
"Joshua",
"Freddie",
"Archie",
"Ethan",
"Isaac",
"Alexander",
"Joseph",
"Edward",
"Samuel",
"Max",
"Daniel",
"Arthur",
"Lucas",
"Mohammed",
"Logan",
"Theo",
"Harrison",
"Benjamin",
"Mason",
"Sebastian",
"Finley",
"Adam",
"Dylan",
"Zachary",
"Riley",
"Teddy",
"Theodore",
"David",
"Toby",
"Jake",
"Louie",
"Elijah",
"Reuben",
"Arlo",
"Hugo",
"Luca",
"Jaxon",
"Matthew",
"Harvey",
"Reggie",
"Michael",
"Harley",
"Jude",
"Albert",
"Tommy",
"Luke",
"Stanley",
"Jenson",
"Frankie",
"Jayden",
"Gabriel",
"Elliot",
"Mohammad",
"Ronnie",
"Charles",
"Louis",
"Elliott",
"Frederick",
"Nathan",
"Lewis",
"Blake",
"Rory",
"Ollie",
"Ryan",
"Tyler",
"Jackson",
"Dexter",
"Alex",
"Austin",
"Kai",
"Albie",
"Caleb",
"Carter",
"Bobby",
"Ezra",
"Ellis",
"Leon",
"Roman",
"Ibrahim",
"Aaron",
"Liam",
"Jesse",
"Jasper",
"Felix",
"Jamie",
"Oliver",
"Noah",
"Liam",
"Benjamin",
"Henry",
"William",
"Logan",
"Alexander",
"Samuel",
"James",
"Mason",
"Daniel",
"Elijah",
"Ethan",
"Lucas",
"Jackson",
"Owen",
"Wyatt",
"Michael",
"Jacob",
"Gabriel",
"Isaac",
"David",
"Aiden",
"Jack",
"Carter",
"Luke",
"Matthew",
"Hunter",
"Hudson",
"Caleb",
"Andrew",
"Levi",
"Sebastian",
"Julian",
"Lincoln",
"Joseph",
"Anthony",
"Jayden",
"Grayson",
"Nathan",
"Jaxon",
"Landon",
"Asher",
"Eli",
"John",
"Joshua",
"Cooper",
"Charles",
"Connor",
"Thomas",
"Dylan",
"Ezra",
"Isaiah",
"Christopher",
"Theodore",
"Nolan",
"Ryan",
"Jonathan",
"Sawyer",
"Josiah",
"Robert",
"Austin",
"Leo",
"Aaron",
"Brayden",
"Jace",
"Parker",
"Evan",
"Mateo",
"Cameron",
"Ryder",
"Gavin",
"Easton",
"Kai",
"Adrian",
"Christian",
"Wesley",
"Jason",
"Colton",
"Elias",
"Declan",
"Silas",
"Angel",
"Ian",
"Jeremiah",
"Miles",
"Emmett",
"Jordan",
"Greyson",
"Adam",
"Jose",
"Maxwell",
"Dominic",
"Bennett",
"Xavier",
"Ryker",
"Bentley",
"Jaxson",
"Zachary",
"Gabriel",
"Adam",
"Raphael",
"Paul",
"Louis",
"Arthur",
"Alexandre",
"Victor",
"Jules",
"Mohamed",
"Lucas",
"Joseph",
"Antoine",
"Gaspard",
"Maxime",
"Augustin",
"Oscar",
"Ethan",
"Leo",
"Leon",
"Martin",
"Hugo",
"Thomas",
"Sacha",
"Noe",
"Noah",
"Clement",
"Liam",
"Rayan",
"Samuel",
"Simon",
"Yanis",
"Nathan",
"Timothée",
"Adrien",
"Axel",
"Enzo",
"Isaac",
"Camille",
"Ismael",
"Naël",
"Basile",
"Côme",
"Charles",
"David",
"Mathis",
"Nolan",
"Leonard",
"Aaron",
"Maël",
"Maxence",
"Eliott",
"Ibrahim",
"Valentin",
"Theo",
"Alexis",
"Baptiste",
"Ulysse",
"Benjamin",
"Marius",
"Youssef",
"Elias",
"Jean",
"Lucien",
"Robin",
"Felix",
"William",
"Gustave",
"Hector",
"Auguste",
"Theodore",
"Gabin",
"Edgar",
"Amir",
"Noam",
"Tom",
"Pierre",
"Ayoub",
"Kaïs",
"Ali",
"Ruben",
"Abel",
"Henri",
"Achille",
"Ilyes",
"Milo",
"Vadim",
"Evan",
"Hadrien",
"Amine",
"Daniel",
"Marceau",
"Nicolas",
"Eden",
"Moussa",
"Antonin",
"Rafael",
"Solal",
"Joshua",
"Mehdi",
"Oliver",
"William",
"Jack",
"Noah",
"Thomas",
"Harrison",
"Ethan",
"Cooper",
"James",
"Henry",
"Mason",
"Hunter",
"Liam",
"Lucas",
"Lachlan",
"Hudson",
"Alexander",
"Levi",
"Charlie",
"Samuel",
"Elijah",
"Max",
"Benjamin",
"Leo",
"Archie",
"Riley",
"Joshua",
"Harry",
"Jacob",
"Oscar",
"Ryan",
"Jaxon",
"Archer",
"Nate",
"Jackson",
"Eli",
"Lincoln",
"Xavier",
"Carter",
"George",
"Isaac",
"Sebastian",
"Tyler",
"Flynn",
"Patrick",
"Hugo",
"Connor",
"Daniel",
"Finn",
"Theodore",
"Ashton",
"Blake",
"Matthew",
"Jake",
"Michael",
"Joseph",
"Braxton",
"Jayden",
"Chase",
"Aiden",
"Edward",
"Hayden",
"Luke",
"Jordan",
"Hamish",
"Luca",
"Dominic",
"Harvey",
"Parker",
"Austin",
"Ryder",
"Beau",
"Logan",
"Nicholas",
"Caleb",
"Kai",
"Seth",
"Darcy",
"Dylan",
"Nathaniel",
"Angus",
"Zachary",
"Charles",
"Jett",
"Mitchell",
"Bailey",
"Lewis",
"Jax",
"Owen",
"Maxwell",
"Fletcher",
"Louis",
"Declan",
"Spencer",
"Wyatt",
"David",
"Nathan",
"Jasper",
"Phoenix",
"Jesse",
"Ben",
"Jonas",
"Leon",
"Elias",
"Finn",
"Noah",
"Paul",
"Luis",
"Lukas",
"Luca",
"Felix",
"Maximilian",
"Henry",
"Max",
"Emil",
"Moritz",
"Jakob",
"Niklas",
"Tim",
"Julian",
"Oskar",
"Anton",
"Philipp",
"David",
"Liam",
"Alexander",
"Theo",
"Tom",
"Mats",
"Jan",
"Matteo",
"Samuel",
"Erik",
"Fabian",
"Milan",
"Leo",
"Jonathan",
"Rafael",
"Simon",
"Vincent",
"Lennard",
"Carl",
"Linus",
"Hannes",
"Jona",
"Mika",
"Jannik",
"Nico",
"Till",
"Johannes",
"Marlon",
"Leonard",
"Benjamin",
"Johann",
"Mattis",
"Adrian",
"Julius",
"Florian",
"Constantin",
"Daniel",
"Aaron",
"Maxim",
"Nick",
"Lenny",
"Valentin",
"Ole",
"Luke",
"Levi",
"Nils",
"Jannis",
"Sebastian",
"Tobias",
"Marvin",
"Joshua",
"Mohammed",
"Timo",
"Phil",
"Joel",
"Benedikt",
"John",
"Robin",
"Toni",
"Dominic",
"Damian",
"Artur",
"Pepe",
"Lasse",
"Malte",
"Sam",
"Bruno",
"Gabriel",
"Lennox",
"Justus",
"Kilian",
"Theodor",
"Oliver",
"Jamie",
"Levin",
"Lian",
"Noel",
"Noah",
"Liam",
"Luca",
"Gabriel",
"Leon",
"David",
"Matteo",
"Elias",
"Louis",
"Levin",
"Samuel",
"Julian",
"Tim",
"Jonas",
"Robin",
"Diego",
"Nico",
"Leo",
"Jan",
"Ben",
"Leandro",
"Dario",
"Lukas",
"Rafael",
"Elia",
"Nino",
"Simon",
"Lenny",
"Gian",
"Benjamin",
"Alessio",
"Fabio",
"Finn",
"Loris",
"Aaron",
"Daniel",
"Lucas",
"Livio",
"Andrin",
"Nevio",
"Leonardo",
"Alexander",
"Nathan",
"Laurin",
"Lian",
"Mattia",
"Enzo",
"Luis",
"Joel",
"Raphael",
"Nils",
"Valentin",
"Lionel",
"Lars",
"Thomas",
"Arthur",
"Luan",
"Joshua",
"Oliver",
"Maximilian",
"Leano",
"Nicolas",
"Felix",
"Marco",
"Dylan",
"Timo",
"Alessandro",
"Mateo",
"Nolan",
"Fabian",
"Damian",
"Noe",
"Adam",
"Maël",
"Maxime",
"Max",
"Ajan",
"Emil",
"Lio",
"Noel",
"Adrian",
"Levi",
"Leo",
"Lorenzo",
"Ryan",
"Paul",
"Ethan",
"Kevin",
"Jason",
"Hugo",
"Kilian",
"Marlon",
"Jonathan",
"Linus",
"Lorik",
"Moritz",
"Lean",
"Manuel",
"Alex",
"Theo",
"Hugo",
"Daniel",
"Pablo",
"Martin",
"Alejandro",
"Adrian",
"Alvaro",
"David",
"Lucas",
"Mario",
"Diego",
"Manuel",
"Leo",
"Mateo",
"Javier",
"Izan",
"Marcos",
"Alex",
"Sergio",
"Marc",
"Carlos",
"Jorge",
"Miguel",
"Enzo",
"Antonio",
"Angel",
"Gonzalo",
"Iker",
"Juan",
"Eric",
"Ivan",
"Ruben",
"Victor",
"Nicolas",
"Bruno",
"Samuel",
"Hector",
"José",
"Gabriel",
"Dario",
"Oliver",
"Aaron",
"Adam",
"Dylan",
"Jesus",
"Marco",
"Aitor",
"Alberto",
"Guillermo",
"Raul",
"Rodrigo",
"Francisco",
"Joel",
"Erik",
"Pau",
"Pedro",
"Luis",
"Jaime",
"Rafael",
"Asier",
"Unai",
"Mohamed",
"Martí",
"Gael",
"Thiago",
"Ian",
"Fernando",
"Oscar",
"Luca",
"Andres",
"Biel",
"Ismael",
"Alonso",
"Pol",
"Nil",
"Jan",
"Rayan",
"Aleix",
"Arnau",
"Cristian",
"Saul",
"Isaac",
"Santiago",
"Julen",
"Joan",
"Miguel",
"Aimar",
"Ignacio",
"Youssef",
"Eduardo",
"Mauro",
"Enrique",
"Yago",
"José",
"Gerard",
"Abraham",
"Noah",
"Omar",
"Ibai",
"Francisco",
"Francesco",
"Alessandro",
"Lorenzo",
"Andrea",
"Leonardo",
"Mattia",
"Matteo",
"Gabriele",
"Riccardo",
"Tommaso",
"Davide",
"Giuseppe",
"Antonio",
"Federico",
"Edoardo",
"Marco",
"Samuele",
"Diego",
"Giovanni",
"Luca",
"Christian",
"Pietro",
"Simone",
"Nicolo'",
"Filippo",
"Alessio",
"Gabriel",
"Michele",
"Emanuele",
"Jacopo",
"Daniele",
"Cristian",
"Giacomo",
"Vincenzo",
"Salvatore",
"Manuel",
"Gioele",
"Thomas",
"Stefano",
"Giulio",
"Samuel",
"Nicola",
"Giorgio",
"Luigi",
"Daniel",
"Elia",
"Angelo",
"Domenico",
"Paolo",
"Raffaele",
"William",
"Lucas",
"Liam",
"Oscar",
"Elias",
"Hugo",
"Oliver",
"Charlie",
"Axel",
"Vincent",
"Alexander",
"Noah",
"Leo",
"Ludvig",
"Adam",
"Arvid",
"Nils",
"Elliot",
"Filip",
"Leon",
"Melvin",
"Viktor",
"Valter",
"Edvin",
"Benjamin",
"Isak",
"Alfred",
"Theo",
"Emil",
"Harry",
"Olle",
"Love",
"Theodor",
"Anton",
"Sixten",
"Erik",
"Adrian",
"Albin",
"Gustav",
"Melker",
"Malte",
"Ebbe",
"Mohamed",
"Gabriel",
"Alvin",
"Max",
"August",
"Josef",
"Viggo",
"Casper",
"Colin",
"Sam",
"Noel",
"Loke",
"Loui",
"Henry",
"Wilmer",
"Kevin",
"Sigge",
"Vidar",
"Carl",
"Jacob",
"Frank",
"Jonathan",
"Matteo",
"Milton",
"Jack",
"Milo",
"Elton",
"Felix",
"Ville",
"Simon",
"Wilhelm",
"Samuel",
"Vilgot",
"Julian",
"Otto",
"Sebastian",
"John",
"Elis",
"Daniel",
"Ivar",
"Joel",
"Tage",
"David",
"Aron",
"Hjalmar",
"Kian",
"Rasmus",
"Eddie",
"Maximilian",
"Algot",
"Linus",
"Alex",
"Ali",
"Edward",
"Ture",
"Elvin",
"Folke",
"Levi",
"William",
"Mathias",
"Oliver",
"Jakob",
"Lucas",
"Filip",
"Liam",
"Aksel",
"Emil",
"Oskar",
"Markus",
"Theodor",
"Elias",
"Alexander",
"Kasper",
"Magnus",
"Jonas",
"Henrik",
"Noah",
"Isak",
"Tobias",
"Sebastian",
"Sander",
"Kristian",
"Leon",
"Daniel",
"Johannes",
"Benjamin",
"Mathéo",
"Nikolai",
"Martin",
"Adrian",
"Mohammad",
"Olav",
"Ludvig",
"Hakon",
"Theo",
"Victor",
"Johan",
"Sondre",
"Erik",
"Felix",
"Andreas",
"Julian",
"Even",
"Sigurd",
"Jonathan",
"Mikkel",
"Herman",
"Iver",
"Oliver",
"Jack",
"William",
"James",
"Benjamin",
"Mason",
"Hunter",
"Charlie",
"Liam",
"Jacob",
"Noah",
"Thomas",
"Max",
"Lucas",
"George",
"Samuel",
"Ryan",
"Alexander",
"Ethan",
"Cooper",
"Carter",
"Jackson",
"Elijah",
"Leo",
"Lachlan",
"Joshua",
"Blake",
"Daniel",
"Henry",
"Isaac",
"Eli",
"Oscar",
"Joseph",
"Braxton",
"Luca",
"Luke",
"Finn",
"Riley",
"Jayden",
"Beau",
"Harry",
"Harrison",
"Levi",
"Lincoln",
"Connor",
"Archie",
"Jaxon",
"Tyler",
"Toby",
"Dylan",
"Santiago",
"Mateo",
"Leonardo",
"Emiliano",
"Diego",
"Santiago",
"Mateo",
"Juan",
"Matias",
"Nicolas",
"Benjamin",
"Pedro",
"Tomas",
"Thiago",
"Santino",
"Haruto",
"Yuto",
"Sota",
"Yuki",
"Hayato",
"Haruki",
"Ryusei",
"Koki",
"Sora",
"Sosuke",
"Riku",
"Soma",
"Ryota",
"Rui",
"Kaito",
"Haru",
"Kota",
"Yusei",
"Yuito",
"Yuma",
"Ren",
"Takumi",
"Minato",
"Eita",
"Shota",
"Daiki",
"Hiroto",
"Kosei",
"Takeru",
"Hinata",
"Toma",
"Manato",
"Ryuki",
"Rikuto",
"Aoto",
"Ibuki",
"Tatsuki",
"Haruma",
"Yamato",
"Ryuto",
"Taisei",
"Yuta",
"Itsuki",
"Soshi",
"Taiga",
"Kosuke",
"Shoma",
"Yushin",
"Ryuga",
"Ryo",
"Rento",
"John",
"Joshua",
"Christian",
"Justin",
"Daniel",
"James",
"Angelo",
"Mark",
"Nathaniel",
"Adrian"
//"海",
//"かい",
//"かずおき",
//"きょうすけ",
//"京也",
//"きよし",
//"キリト",
//"きわむ",
//"ぎん",
//"くに",
//"くにお",
//"けい",
//"げん",
//"けんと",
//"けんいち",
//"ごう",
//"康一",
//"こうじ",
//"コウジ",
//"こよみ",
//"五郎",
//"カイ",
//"香織",
//"かずね",
//"かずみ",
//"和美",
//"かのん",
//"カリン",
//"きょうこ",
//"杏子",
//"けい",
//"こよみ",
//"ぐり子",
//"くれあ",
//"ここあ",
//"ことり",
//"このみ",
//"小鳩",
//"абаза бызшва",
//"abaza bəzš˚a",
//"Abenaki Alnôba",
//"Abkhaz  аҧсуа бызшәа",
//"Abui",
//"Achang",
//"Bahsa Acèh",
//"Acheron",
//"ís siwa wó disi",
//"Tenánat Hadéyas",
//"Fulfulde",
//"adəgăbză",
//"Adzera",
//"ʿAfár af",
//"Afrikaans",
//"агъул чӀал",
//"Qa'yol",
//"Awajún",
//"Ahom",
//"Waawilûû",
//"akan",
//"Ašval'i mic'i",
//"𒀝𒅗𒁺𒌑",
//"Anicinâbemowin",
//"Ëlsässisch",
//"Алтай тили",
//"ኣማርኛ",
//"мицци",
//"Σάννα",
//"مصري‎",
//"اللغة المصرية الحديثة",
//"تونسي",
//"ရခိုင်ဘာသာ",
//"לשנא ארמיא",
//"ܠܫܢܐ ܤܘܪܝܝܐ",
//"অসমীয়া",
//"къIaваннаб мицци",
//"къарачай-малкъар тил",
//"ბაცბურ მოტტ",
//"বাংলা",
//"бежкьалас миц",
//"भोजपुरी",
//"босански",
//"Буйхалъи мицӏцӏи",
//"Bouyei  Haasqyaix",
//"български",
//"български език",
//"Bundjalung",
//"ဗမာစကား",
//"Нохчийн мотт",
//"廣東話",
//"粵語",
//"东干语",
//"赣语",
//"江西话",
//"客家話",
//"客話",
//"土廣東話",
//"𠊎話",
//"上海闲话",
//"ᑌᓀᓲᒢᕄᓀ",
//"marilenghe",
//"zeneisei",
//"עִבְרִית",
//"كٲشُر",
//"Nama",
//"کوردی",
//"к’öрди",
//"Nedderdüütsch",
//"Neddersassisch",
//"Kajin Majōl",
//"faka-Niue",
//"român",
//"sámegiellaa",
//"sardu",
//"Lallans",
//"Doric",
//"ślůnski",
//"Dene-thah",
//"Dené Dháh",
//"Dene Zhatıé",
//"Spanish español",
//"castellano",
//"toçikī",
//"تاجيكي",
//"Tamahaq Tamahaq",
//"Tyva dyl",
//"Twi twi",
//"Vahcuengh",
//"Zulu"
        };

        string nameComp = nameComponent1[Random.Range(0, nameComponent1.Length)].ToString();
        randomName = nameComp;
    }
}
