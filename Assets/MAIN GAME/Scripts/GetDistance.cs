﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetDistance : MonoBehaviour
{
    public static float totalDistance;
    public PathFollow followPath;
    // Start is called before the first frame update
    void Start()
    {
        followPath.target = GameController.instance.currentMap;
        followPath.distance = 0;
        followPath.animate = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            GameController.instance.progress.maxValue = followPath.distance;
            GameController.instance.progress1.maxValue = followPath.distance;
            GameController.instance.progress2.maxValue = followPath.distance;
            PlayerMovement.defaultSpeed = 100/followPath.distance;
            followPath.animate = false;
        }
    }
}
