﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialDetect : MonoBehaviour
{
    public GameObject tutorial;
    int isFirstTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        isFirstTime = PlayerPrefs.GetInt("isTutorial");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Red"))
        {
            if (isFirstTime == 0)
            {
                isFirstTime++;
                tutorial.SetActive(true);
                tutorial.GetComponent<Text>().text = "Avoid the red";
                StartCoroutine(delaySlow());
                PlayerPrefs.SetInt("isTutorial", 1);
            }
        }
        if (other.CompareTag("Obstacle"))
        {
            if (isFirstTime == 1)
            {
                isFirstTime++;
                tutorial.SetActive(true);
                tutorial.GetComponent<Text>().text = "Hold to break";
                StartCoroutine(delaySlow());
                PlayerPrefs.SetInt("isTutorial", 2);
            }
        }
    }

    IEnumerator delaySlow()
    {
        PlayerMovement.worldPathDeform.animate = false;
        yield return new WaitForSeconds(3);
        PlayerMovement.worldPathDeform.animate = true;
        tutorial.SetActive(false);
    }
}
